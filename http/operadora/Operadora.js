var mongoose = require('mongoose');

var operadoraSchema = new mongoose.Schema({
    nome: {
        type: String
    },
    codigo: {
        type: Number
    },
    preco: {
        type: Number
    },
    categoria: {
        type: String
    } 
});

module.exports = mongoose.model('Operadora', operadoraSchema);