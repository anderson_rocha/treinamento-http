var Operadora = require('./Operadora.js');

var operadoraService = function () {
    var operadoras = [{
            nome: "TIM",
            preco: "390",
            categoria: "Mobile",
            codigo: "41"
        },
        {
            nome: "VIVO",
            preco: "240",
            categoria: "Mobile",
            codigo: "11"
        },
        {
            nome: "Claro",
            preco: "59",
            categoria: "Fixo",
            codigo: "21"
        }
    ];

    var addOperadoras = function (req, res, body) {
        Operadora.findOneAndUpdate({
            "nome": body.nome
        }, body, {
            upsert: true
        }, function (err, docs) {
            if (err) {
                console.log("Erro");
            }
            res.json(docs);
            res.end();
        });
    }

    var removeOperadora = function (req, res, body) {
        Operadora.remove({"nome": body.nome}, (err) => {
            if(err) {
                console.log(err);
            } else
                res.write("Operadora removida:" + body);
        });
        res.end();
    }

    var atualizarOperadora = function (operadoraAntiga, operadoraNova) {
        let pos = operadoras.map(elemento => elemento.nome).indexOf(operadoraAntiga.nome);
        operadoras.splice(pos, 1, operadoraNova);
        return operadoras;
    }

    var retornarOperadoras = function (req, res) {
        console.log("GET");
        Operadora.find({}, function (err, operadorasDb) {
            res.json(operadorasDb);
        });
    }

    return {
        atualizarOperadora: atualizarOperadora,
        removeOperadora: removeOperadora,
        retornarOperadoras: retornarOperadoras,
        addOperadoras: addOperadoras
    };
}

module.exports = operadoraService();