var router = require('./router/Router.service.js');
var contatos = require('./contato/Contatos.service.js');
var operadoras = require('./operadora/Operadora.service.js');
var express = require('express');
require('./config/database');
var app = express();

app.get('/', function(req, res) {
    res.write( '<h1>Home</h1>');
    res.end();
});


app.post("/contatos", function (req, res) {
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        contatos.addContatos(req, res, JSON.parse(body));
    });    
});

app.get("/contatos", function (req, res) {
    contatos.retornarContatos(req, res);
});

app.delete("/contatos", function (req, res) {
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        contatos.removeContatos(req, res, JSON.parse(body));
    });
});

app.put("/contatos", function(req, res) {
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        contatos.addContatos(req, res, JSON.parse(body));
    });
});

app.delete("/operadoras", function (req, res){
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        operadoras.removeOperadora(req, res, JSON.parse(body));
    });
});

app.post("/operadoras", function (req, res) {
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        operadoras.addOperadoras(req, res, JSON.parse(body));
    });
});

app.get("/operadoras", function (req, res) {
    operadoras.retornarOperadoras(req, res);

});

app.put("/operadoras", function (req, res){
    let body = '';
    req.on('data', function (chunk){
        body += chunk.toString();
    });
    req.on('end', function () {
        operadoras.addOperadoras(req, res, JSON.parse(body));
    });
});

app.listen(3432, () => console.log("Funcionando na porta 3432"));