const database = require('./Mongo.service.js');

var databaseService = function () {
    var logDatabase = function() {
        database.then(function(db){
            console.log(db);
            const collection = db.collection('operadoras');
    
            collection.find({}).toArray(function (err, docs){
                console.log(docs);
            });
        });
    }
    
    return {
        logDatabase: logDatabase
    }
}

module.exports = databaseService();