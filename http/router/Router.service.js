var routerService = function () {
    var http = require('http');

    var createRouter = function (port) {
        var routes = {
            'GET': {},
            'POST': {},
            'DELETE': {},
            'PUT': {}
        };

        var get = function (path, callback) {
            routes['GET'][path] = callback;
        }

        var post = function(path, callback) {
            routes['POST'][path] = callback;
        }

        var del = function (path, callback) {
            routes['DELETE'][path] = callback;
        }

        var put = function (path, callback) {
            routes['PUT'][path] = callback;
        }

        http.createServer(function (req, res) {
            if(!routes[req.method][req.url]) {
                res.end();
            } else {
                console.log("Método "+ req.method);
                console.log("URL "+ req.url);
                console.log(routes[req.method][req.url]);
                routes[req.method][req.url](req, res);
            }
        }).listen(port);

        return {
            put: put,
            get: get,
            post: post,
            del: del
        }
    }

    return {
        createRouter: createRouter
    }
}

module.exports = routerService();