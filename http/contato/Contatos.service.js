var operadora = require('../operadora/Operadora.service.js');
var Contato = require('./Contato.schema.js');

var contatos = function () {
    var contatos = [{
            _id: 1,
            nome: "Bruno",
            numero: "9999-9999",
            operadora: {
                nome: "TIM",
                preco: 390,
                categoria: "Mobile",
                codigo: 41
            }
        },
        {
            _id: 2,
            nome: "Anderson",
            numero: "9922-9999",
            operadora: {
                nome: "VIVO",
                preco: 240,
                categoria: "Mobile",
                codigo: 11
            }
        }
    ];

    var retornarContatos = function (req, res) {
        Contato.find({}, function (err, docs) {
            res.json(docs);
            res.end();
        });
    }

    var addContatos = function (req, res, body) {
        Contato.findOneAndUpdate({
            "_id": body._id
        }, body, {
            upsert: true
        }, function (err, docs) {
            console.log("Docs " + docs);
            if (err) {
                console.log("Erro " + err);
            }
            res.json(docs);
            res.end();
        });
    }

    var atualizarContatos = function (contatoAntigo, contatoNovo) {
        let pos = contatos.map(elemento => elemento.id).indexOf(contatoAntigo.id);
        contatos.splice(pos, 1, contatoNovo);
        return contatos;
    }

    var removeContatos = function (req, res, body) {
        Contato.remove({"_id": body._id}, (err) => {
            if(err) {
                console.log(err);
            } else
                res.write("Contato removido:" + body);
        });
        res.end();
    }

    return {
        atualizarContatos: atualizarContatos,
        addContatos: addContatos,
        retornarContatos: retornarContatos,
        removeContatos: removeContatos
    }
}

module.exports = contatos();