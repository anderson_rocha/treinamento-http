var mongoose = require('mongoose');
var operadoraSchema = require('../operadora/Operadora.js').schema;

var contatoSchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    nome: {
        type: String
    },
    numero: {
        type: String
    },
    operadora: {
        type: [operadoraSchema]
    }
});

module.exports = mongoose.model('Contato', contatoSchema);