var createGlobalObject = function() {

    var getGlobal = function () {
        return global;
    }
    var teste = function () {
        return "Bruno";
    }

    return {
        getGlobal: getGlobal,
        teste: teste
    }
};

module.exports = createGlobalObject;