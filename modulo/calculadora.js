var calculadora = function () {
    var somar = function (a, b) {
        return a+b;
    }

    var subtrair = function (a, b) {
        return a-b;
    }

    var multiplicar = function (a, b) {
        return a*b;
    }
    
    var dividir = function(a, b) {
        return a/b;
    }
    return {
        somar: somar,
        subtrair: subtrair,
        multiplicar: multiplicar,
        dividir: dividir,
    };
}
module.exports = calculadora(); 
