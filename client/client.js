var http = require('http');
const fetch = require('node-fetch');
const request = require('request');

// var getContatos = function () {
//     http.get('http://localhost:3432/contatos', (res) => {
//         let data = '';
//         console.log('Resp ' + res);
//         res.on('data', (chunk) => {
//             data += chunk;
//         });
//         res.on('end', (res) => {
//             return JSON.parse(data);
//         });
//     }).on('error', (err) => {
//         console.log('Erro ' + err);
//     });
// }
var testandoPromises = () => {
var promises = [];
    var promise = new Promise((resolve, reject) => {
        request('http://google.com.br', (err, response, body) => {
            if (err)
                reject(err);
                console.log('Entrei no Google');
            resolve(response, body);
        });
    });
    promise.then((response, body) => {
        request('http://amazon.com.br', (err, response, body) => {
            // console.log(body);
            console.log('Entrei na Amazon');
        });
    });
};

var getOperadoras = function () {
    return fetch('http://localhost:3432/operadoras').then((res) => {
        return res.json();
    })
        .catch((err) => {
            console.log('Erro ' + err);
        });
}

// getContatos().then((contatos)=> {
//     console.log(contatos);
// });

// getOperadoras().then((operadoras) => {
//     console.log(operadoras);
// });

testandoPromises();
