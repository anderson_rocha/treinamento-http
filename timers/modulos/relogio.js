var relogio = function () {
    var imprimirData = function (tempo) {
        setTimeout(function() {console.log(new Date().toUTCString())}, tempo);
    }

    var imprimirDataRecursivamente = function(time) {
        setTimeout(function () {
            console.log("Olá " + new Date().toUTCString());
            imprimirDataRecursivamente(time);
        }, time); 
    }

    var imprimirDataInterval = function() {
        setInterval(function() {
            console.log("Recursivo " + new Date().toUTCString());
        }, 1000);
    }

    return {
        imprimirData: imprimirData,
        imprimirDataRecursivamente: imprimirDataRecursivamente,
        imprimirDataInterval: imprimirDataInterval
    }
}

module.exports = relogio();